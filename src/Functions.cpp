/*
 * Functions.cpp
 *
 *  Created on: 18.11.2014
 *      Author: rikimaru
 */

#include "functions.h"

b2Body* createBox2dObject(b2World* world, b2BodyDefProperties prop, binaryPare position, binaryPare shapeSize, fixtureParams fParams) {
	b2BodyDef obj;
	obj.type = prop.type;
	obj.fixedRotation = prop.isFixedRotation;
	obj.position.Set(position.x, position.y);

	b2Body* body = world->CreateBody(&obj);

	b2PolygonShape shape;
    if (prop.type == b2_dynamicBody && fParams.density == 0.03f)
    {
        b2Vec2 vertic[8];
        vertic[0].Set(2 * METRE_TO_PIX, 0);
        vertic[1].Set(25 * METRE_TO_PIX, 0);
        vertic[2].Set(27 * METRE_TO_PIX, 2 * METRE_TO_PIX);
        vertic[3].Set(27 * METRE_TO_PIX, 45 * METRE_TO_PIX);
        vertic[4].Set(25 * METRE_TO_PIX, 47 * METRE_TO_PIX);
        vertic[5].Set(2 * METRE_TO_PIX, 47 * METRE_TO_PIX);
        vertic[6].Set(0, 45 * METRE_TO_PIX);
        vertic[7].Set(0, 2 * METRE_TO_PIX);
        shape.Set(vertic, 8);
    }
    else if (prop.type == b2_dynamicBody && (fParams.density == 0.01f || fParams.density == 0.02f)) {
        b2Vec2 vertic[8];
        vertic[0].Set(2 * METRE_TO_PIX, 0);
        vertic[1].Set(28 * METRE_TO_PIX, 0);
        vertic[2].Set(30 * METRE_TO_PIX, 2 * METRE_TO_PIX);
        vertic[3].Set(30 * METRE_TO_PIX, 28 * METRE_TO_PIX);
        vertic[4].Set(28 * METRE_TO_PIX, 30 * METRE_TO_PIX);
        vertic[5].Set(2 * METRE_TO_PIX, 30 * METRE_TO_PIX);
        vertic[6].Set(0, 28 * METRE_TO_PIX);
        vertic[7].Set(0, 2 * METRE_TO_PIX);
        shape.Set(vertic, 8);
    }
    else    {
        shape.SetAsBox(16 * METRE_TO_PIX, 16 * METRE_TO_PIX);
    }
	b2FixtureDef fixture;
	fixture.shape = &shape;
	fixture.density = fParams.density;
	fixture.friction = fParams.friction;

	body->CreateFixture(&fixture);
	return body;
}

bool SBT::inArray(char* arr, char val, int size) {
    for( int i = 0; i < size; i++ ) {
        if( arr[i] == val ) {
            return true;
        }
    }
    return false;
}
