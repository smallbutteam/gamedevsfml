//
//  Scene.h
//  GameProject
//
//  Created by Andrey Bondar on 09.11.14.
//  Copyright (c) 2014 Andrey Bondar. All rights reserved.
//

#ifndef GameProject_Scene_h
#define GameProject_Scene_h

#include "level.h"
#include "builder.h"

using namespace sf;

namespace SBT {
    
    class Scene {
    private:
        Level *CompanionLvl;
        Level *PlayerLvl;
        Sync* sync;
    public:
        Scene(int levelId);
        void update(RenderWindow& window, Event& event);
        Network* Connection;
    };
    
}

#endif
