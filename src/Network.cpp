//
//  Network.cpp
//  GameProject
//
//  Created by Andrey Bondar on 12.11.14.
//  Copyright (c) 2014 Andrey Bondar. All rights reserved.
//

#include "network.h"
#include "chooseInterface.h"

using namespace sf;

const int PORT = 5500;

SBT::Network::Network() {
    bool choiseSuccess = false;
    do {
        choiseSuccess = makeChoise();
    } while( !choiseSuccess );
}

bool SBT::Network::makeChoise() {
    myIP = IpAddress::getLocalAddress();
    std::cout << myIP.toString() << std::endl;
    
    int act = 0;
    RenderWindow w( VideoMode(400, 300), "Test!");
    act = chooseAction(w);
    
    bool success = false;
    
    if( act == 1 ) {
        waitForFriendWindow(w);
        success = createHost();
    }
    
    if( act == 2 ) {
        std::string ip = inputIp(w);
        success = joinHost(ip);
    }
    
    return success;
}

bool SBT::Network::createHost() {
    std::cout << "Ожидание присоединения напарника" << std::endl;
    if( this->listenHost.listen(PORT) != Socket::Done) {
        std::cout << "Ошибка прослушки сокета!" << std::endl;
        return false;
    }
   
    if( this->listenHost.accept(this->Client) != Socket::Done) {
        std::cout << "Ошибка ожидания присоединения напарника";
        return false;
    }
    return true;
}

bool SBT::Network::joinHost(std::string otherIp) {
    std::cout << "Соединение..." << std::endl;
    Socket::Status status = this->Client.connect(otherIp, PORT);
    if( status != Socket::Done ) {
        std::cout << "Ошибка соединения" << std::endl;
        return false;
    }
    std::cout << "connect ok" << std::endl;
    return true;
}

void SBT::Network::sendData(char Data) {
    if( this->Client.send(&Data, 1) != Socket::Done) {
        std::cout << "Не удалось передать данные" << std::endl;
    }
}

char SBT::Network::reciveData() {
    std::size_t recived;
    char Data = 0;
    if( this->Client.receive(&Data, 1, recived) != Socket::Done) {
        return 0;
    }
    return Data;
}