/*
 * level.h
 *
 *  Created on: 08 нояб. 2014 г.
 *      Author: rscprof
 */

#ifndef LEVEL_H_
#define LEVEL_H_

#include <SFML/Graphics.hpp>
#include "player.h"
#include "eventCatcher.h"
#include "builder.h"
#include "network.h"
#include "sync.h"

const int ARROW_SIZE = 31;

namespace SBT {
    
	class Sync;
        
    class Signal {
    public:
        sf::Texture signalsTileset;
        sf::Sprite signal;
        sf::Clock timer;
        bool showed;
        Signal();
        bool isShowed();
        
    };
    
	class Level {
	private:
		float offsetX;
		float offsetY;
        
        bool isLocal;

		std::string* TileMap;

		SBT::Player* p;
        
        sf::Sprite background;
		sf::Texture tileSet;
        sf::Texture backgroundSet;
		sf::Clock clock;
        sf::Clock backgroundClock;

        Signal* s;
    
		int ScreenStart;
		int CameraCalibration;
        
        char backgroundNumber;
        int backgroundY;

		EventCatcher *catcher;
		Network* connection;
	public:
		Level(int levelId, bool isPlayer, Network* pConnection);
		void update(sf::RenderWindow& window, Sync* sync,bool flag);
		void redraw(sf::RenderWindow& window);
		b2World *world;
        int backgroundShift;
	};

}

#endif /* LEVEL_H_ */
