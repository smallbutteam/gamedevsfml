#ifndef player_cpp
#define player_cpp

#include "player.h"

using namespace sf;

SBT::Player::Player(std::string* TileMap, float* offsetX, float* offsetY, bool isPlayer, b2World *pWorld) : playerBody(0) {
    this->TileMap = TileMap;
    this->offsetX = offsetX;
    this->offsetY = offsetY;
    if (isPlayer)
    {
        ScreenStart = HEIGHT_VISION;
    }
    else {
        ScreenStart = 0;
    }

    if(!this->texture.loadFromFile("res/img/Player.png")){
        this->texture.loadFromFile("../res/img/Player.png");
    }
    
//    if( !this->tiles.loadFromFile("res/img/tiles01.bmp") ) {
//        this->tiles.loadFromFile("../res/img/tiles01.bmp");
//    }


    ScreenStart = (isPlayer) ? HEIGHT_VISION : 0;

    this->look = FRONT;
    this->sprite.setTexture(this->texture);
    this->world = pWorld;
    draw = 1;
    dying = 0;
    for (int y = 0; y < H; y++)
    {
        for(int x = 0; x < W; x++)
        {
            if (TileMap[y][x] == 'P') {
            	this->playerBody = createBox2dObject(
						this->world,
						b2BodyDefProperties(true, b2_dynamicBody),
						binaryPare(x*32.0f * METRE_TO_PIX, METRE_TO_PIX * (y*32.0f  + this->ScreenStart)),
						binaryPare(PLAYER_WIDTH/2 * METRE_TO_PIX , PLAYER_HEIGHT/2 * METRE_TO_PIX ),
						fixtureParams(0.03f, 1.0f, 0.01f)
            	);
			}
        }
        
    }
}

	void SBT::Player::update(float time, EventCatcher *catcher, bool flag, int& backgroundShift) {
        if (flag){
            if ( this->playerBody->GetPosition().y * PIX_TO_METRE < 665 ){
                *this->offsetY = *this->offsetY - (665 - PIX_TO_METRE * this->playerBody->GetPosition().y) ;
            }
        } else {
            if ( this->playerBody->GetPosition().y * PIX_TO_METRE < 295 ){
                *this->offsetY = *this->offsetY - (295 - PIX_TO_METRE * this->playerBody->GetPosition().y) ;
            }
        }
        if ( this->playerBody->GetPosition().x * PIX_TO_METRE < 600){
            *this->offsetX = this->playerBody->GetPosition().x * PIX_TO_METRE ;
        }
        if(this->onGround)
        {
            playerBody->SetLinearVelocity(b2Vec2(0.0f, playerBody->GetLinearVelocity().y));
        }
        if (!dying)
        {
            if (catcher->moveLeft()){

              this->playerBody->SetLinearVelocity(b2Vec2(-2.0f, this->playerBody->GetLinearVelocity().y));
    //
                
                this->dx = -100.0f;
                if (this->playerBody->GetPosition().x * PIX_TO_METRE > 600) {
                    backgroundShift -= 1;
                }
            }
            if (catcher->moveRight()){
                this->playerBody->SetLinearVelocity(b2Vec2(2.0f, this->playerBody->GetLinearVelocity().y));
                this->dx = 100.0f;
                if (this->playerBody->GetPosition().x * PIX_TO_METRE > 600) {
                    backgroundShift += 1;
                }
            }
            if (catcher->jump()){
                if( this->onGround ) {
                    float impulse = 0.11f;
                    playerBody->ApplyLinearImpulse(b2Vec2(0, -impulse), playerBody->GetWorldCenter(), false);
                    this->onGround = false;
                }
            }
        }
        
        
        onGround = false;
        for( b2Body* MagneticSearch = world->GetBodyList(); MagneticSearch; MagneticSearch = MagneticSearch->GetNext())
        {
            b2Fixture* tempFix =  MagneticSearch->GetFixtureList();
            if (tempFix->GetDensity() == 1.4f)
            {
                for (b2Body* DynamicBody = world->GetBodyList(); DynamicBody; DynamicBody = DynamicBody->GetNext())
                {
                    if (DynamicBody->GetType() == b2_dynamicBody)
                    {
                        if( (DynamicBody->GetPosition().x + 5 > MagneticSearch->GetPosition().x)
                           && (DynamicBody->GetPosition().x - 5 < MagneticSearch->GetPosition().x))
                        {
                            float Dif = DynamicBody->GetPosition().x + PLAYER_WIDTH/2 * METRE_TO_PIX
                            - MagneticSearch->GetPosition().x;
                            DynamicBody->ApplyForce(b2Vec2(-Dif*0.2f, 0), DynamicBody->GetWorldCenter(), true);
                            
                            if( (DynamicBody->GetPosition().y + 5 > MagneticSearch->GetPosition().y)
                               && (DynamicBody->GetPosition().y - 5 < MagneticSearch->GetPosition().y))
                            {
                                float Dif = DynamicBody->GetPosition().y + PLAYER_HEIGHT/2 * METRE_TO_PIX
                                - MagneticSearch->GetPosition().y;
                                DynamicBody->ApplyForce(b2Vec2(0, -Dif*0.02f), DynamicBody->GetWorldCenter(), true);
                            }
                        }
                    }
                }
            }
        }
        
        
        for( b2ContactEdge* ce = playerBody->GetContactList(); ce; ce = ce->next)
        {
            b2Contact* c = ce->contact;
            b2Fixture* tempFix = c->GetFixtureA();
            for( b2Body* Bodyiter = world->GetBodyList(); Bodyiter; Bodyiter = Bodyiter->GetNext())
            {
                if( Bodyiter->GetType() == b2_staticBody)
                {
                    if( (tempFix == Bodyiter->GetFixtureList()) && (tempFix->GetDensity() == 1.1f))
                    {
                        //std::cout << (playerBody->GetPosition().y - Bodyiter->GetPosition().y) * PIX_TO_METRE << std::endl;
                        if ((playerBody->GetPosition().y * PIX_TO_METRE + 60 < Bodyiter->GetPosition().y * PIX_TO_METRE )
                           && (playerBody->GetLinearVelocity().y ==0))
                        {
                        //    std::cout << "fuck this shit" << std::endl;
                            onGround = true;
                        }
//                        if (tempFix->GetFriction() == 0.01f)
//                        {
//                            //std::cout << "test";
//                            b2Vec2 vec = Bodyiter->GetPosition();
//                            //sf::Sprite showSp;
//                            showSp.setTexture(tiles);
//                            showSp.setTextureRect(IntRect(0,0,32,32));
//                            showSp.setColor(Color::Cyan);
//                            b2Vec2 vec2;
//                            vec2.x = vec.x - this->playerBody->GetPosition().x + (*this->offsetX - 16) * METRE_TO_PIX;
//                            vec2.y = vec.y - this->playerBody->GetPosition().y  + (*this->offsetY - 16) * METRE_TO_PIX;
//                            showSp.setPosition(vec2.x * PIX_TO_METRE, vec2.y * PIX_TO_METRE);
//                            showSp.setRotation(Bodyiter->GetAngle() * 180/b2_pi);
//                            
//                        }
                    }
                    else if ( (tempFix == Bodyiter->GetFixtureList()) && (tempFix->GetDensity() == 1.2f))
                    {
                        Bodyiter->GetWorld()->DestroyBody(Bodyiter);
                    }
                    else if ( (tempFix == Bodyiter->GetFixtureList()) && (tempFix->GetDensity() == 1.3f))
                    {
    //                    if (dying == 0)
    //                    {
    //                        //dying = 50;
    //                    }
    //                    if (!(dying % 5))
    //                    {
    //                        draw = 0;
    //                    }
    //                    else {
    //                        draw = 1;
    //                    }
    //                    dying--;
    //                    if (!dying)
                        playerBody->SetTransform(b2Vec2(5*32 * METRE_TO_PIX, METRE_TO_PIX * (7*32 + this->ScreenStart)), 0);
                    }
                }
                else if( Bodyiter->GetType() == b2_dynamicBody)
                {
                    //b2Fixture* tempFix = Bodyiter->GetFixtureList();
//                    if(( tempFix == Bodyiter->GetFixtureList()) && (tempFix->GetDensity() == 0.02f))
//                    {
//                        //std::cout << "a";
//                        if (playerBody->GetPosition().y * PIX_TO_METRE + 15 > Bodyiter->GetPosition().y * PIX_TO_METRE )
//                        {
//                            //float vec = playerBody->GetLinearVelocity().x;
//                            //playerBody->SetLinearVelocity(b2Vec2(vec, 0));
//                            std::cout << "b" << std::endl;
//                            onGround = true;
//                        }
//                    }
                }
            }
        }
        
        this->updateTexture(time);
        if (draw)
            this->sprite.setPosition( (*this->offsetX), (*this->offsetY));
		this->dx=0;
	}

	void SBT::Player::updateTexture(float time) {

		if(!onGround) {
			if ( this->dx > 0) {
				this->look = RIGHT;
				this->sprite.setTextureRect(IntRect(334, 204, PLAYER_WIDTH, PLAYER_HEIGHT));
			} else if( this->dx < 0) {
				this->look = LEFT;
				this->sprite.setTextureRect(IntRect(338, 76, PLAYER_WIDTH, PLAYER_HEIGHT));
			} else {
				this->look = FRONT;
				this->sprite.setTextureRect(IntRect(17, 140, PLAYER_WIDTH, PLAYER_HEIGHT));
			}
		} else {

			this->currentFrame += 0.005*time;
			if( currentFrame > 6 ) {
				currentFrame -= 6 ;
			}

			if( this->dx > 0 ) {
				this->look = RIGHT;
				this->sprite.setTextureRect(IntRect(64*int(currentFrame)+17, 717, PLAYER_WIDTH, PLAYER_HEIGHT));
			}
			if( this->dx < 0 ) {
				this->look = LEFT;
				this->sprite.setTextureRect(IntRect(64*int(currentFrame)+17, 589, PLAYER_WIDTH, PLAYER_HEIGHT));
			}
			if( this->dx == 0 ) {
				this->look = FRONT;
				this->sprite.setTextureRect(IntRect(17, 140, PLAYER_WIDTH, PLAYER_HEIGHT));
			}
		}
	}

#endif
