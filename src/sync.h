/* 
 * File:   sync.h
 * Author: rikimaru
 *
 * Created on 12 Декабрь 2014 г., 0:49
 */

#ifndef SYNC_H
#define	SYNC_H

#include "builder.h"

namespace SBT{
    
    class Level;
    
    class Sync {
	private:
		Level* localLevel;
		Level* remoteLevel;
		Network* connection;
	public:
		Sync(Level* local, Level* remote, Network* pConnection);

    };
}


#endif	/* SYNC_H */

