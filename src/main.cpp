#include "scene.h"


using namespace sf;
using namespace std;

using namespace SBT;

int main(){

    RenderWindow window( VideoMode(VIDEOMODE_X, VIDEOMODE_Y), "Test!");
    Event event;
    Scene Sc(1);
    while (window.isOpen()){
    	Sc.update(window, event);
    }

    return 0;
}

std::string TILE_MAP[][40]= {

	// B - Блок
	// 0 - Съедающийся блок
	// U - Плитка
	// H - Активация плитки
	// S - Spike (Ловушка)

	{
		"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",
		"B                          B           B",
		"B  0   0   00000  00000   B 0     0   0B",
		"B  00 00     0    0      B 0 0    00 00B",
		"B  0 0 0     0    00BBB   00000   0 0 0B",
		"B  0   0   B 0 BBB0      0     0  0   0B",
		"B  0   0  B  0    00000 0       0 0   0B",
		"BBB      B               B             B",
		"B    B                B    B  B        B",
		"B    B              P  B   BHHB        B",
		"B    B          UU    B    BSSB        B",
		"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",
	},


	{
		"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",
		"B                     B                       BM     M     BBBBBBBBBBBBBBBBBB",
		"B                     B                       B            BBBBBBBBBBBBBBBBBB",
		"B                     B     F                 B   D     D  BBBBBBBBBBBBBBBBBB",
		"B            BBB      B     BBB               B            BBBBBBBBBBBBBBBBBB",
		"B                     B                       B            BBBBBBBBBBBBBBBBBB",
		"B         B           B           HHH         BM          MBBBBBBBBBBBBBBBBBB",
		"B                                             B            BBBBBBBBBBBBBBBBBB",
		"B   P        B   B                            B            BBBBBBBBBBBBBBBBBB",
		"B        F   B   B          HHH               B   D     D  BBBBBBBBBBBBBBBBBB",
		"B       FF   BSSSB                            B      M    MBBBBBBBBBBBBBBBBBB",
		"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",
        	"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",
        //	"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB",
	}

};

