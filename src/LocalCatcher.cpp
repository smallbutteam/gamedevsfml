//
//  LocalCatcher.cpp
//  GameProject
//
//  Created by Andrey Bondar on 10.11.14.
//  Copyright (c) 2014 Andrey Bondar. All rights reserved.
//

#include "localCatcher.h"
#include "eventCatcher.h"

using namespace sf;

SBT::LocalCatcher::LocalCatcher(Network* pNetwork) {
    this->network = pNetwork;
}

bool SBT::LocalCatcher::moveLeft()
{
    if (Keyboard::isKeyPressed(Keyboard::Left)){
        this->network->sendData('L');
        return true;
    }
    else if( Keyboard::isKeyPressed(Keyboard::A) ) {
        this->network->sendData('L');
        return true;
    }
    else {
        this->network->sendData(' ');
        return false;
    }
}

bool SBT::LocalCatcher::moveRight() {
    if( Keyboard::isKeyPressed(Keyboard::Right) ) {
        this->network->sendData('R');
        return true;
    }
    else if( Keyboard::isKeyPressed(Keyboard::D) ) {
        this->network->sendData('R');
        return true;
    }
    else {
        this->network->sendData(' ');
        return false;
    }
}

bool SBT::LocalCatcher::jump() {
    if( Keyboard::isKeyPressed(Keyboard::Up) ) {
        this->network->sendData('J');
        return true;
    }
    else if( Keyboard::isKeyPressed(Keyboard::W) ) {
        this->network->sendData('J');
        return true;
    }
    else {
        this->network->sendData(' ');
        return false;
    }
}

bool SBT::LocalCatcher::showUp() {
    if( Keyboard::isKeyPressed(Keyboard::U) ) {
        this->network->sendData('U');
        return true;
    }
    else {
        this->network->sendData(' ');
        return false;
    }
}

bool SBT::LocalCatcher::showLeft() {
    if( Keyboard::isKeyPressed(Keyboard::H) ) {
        this->network->sendData('H');
        return true;
    }
    else {
        this->network->sendData(' ');
        return false;
    }
}

bool SBT::LocalCatcher::showRight() {
    if( Keyboard::isKeyPressed(Keyboard::K) ) {
        this->network->sendData('K');
        return true;
    }
    else {
        this->network->sendData(' ');
        return false;
    }
}

bool SBT::LocalCatcher::showUpLeft() {
    if( Keyboard::isKeyPressed(Keyboard::Y) ) {
        this->network->sendData('Y');
        return true;
    }
    else {
        this->network->sendData(' ');
        return false;
    }
}

bool SBT::LocalCatcher::showUpRight() {
    if( Keyboard::isKeyPressed(Keyboard::I) ) {
        this->network->sendData('I');
        return true;
    }
    else {
        this->network->sendData(' ');
        return false;
    }
}