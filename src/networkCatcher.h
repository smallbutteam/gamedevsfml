//
//  NetworkCatcher.h
//  GameProject
//
//  Created by Andrey Bondar on 10.11.14.
//  Copyright (c) 2014 Andrey Bondar. All rights reserved.
//

#ifndef __GameProject__NetworkCatcher__
#define __GameProject__NetworkCatcher__

#include "eventCatcher.h"
#include <queue>

namespace SBT {
    
    class NetworkCatcher : public EventCatcher {
    private:
        std::queue<char> lastData;
    public:
        NetworkCatcher(Network* network);
        bool receiveSignal();
        bool moveLeft();
        bool moveRight();
        bool jump();
        bool showUp();
        bool showLeft();
        bool showRight();
        bool showUpLeft();
        bool showUpRight();
    };
    
}

#endif /* defined(__GameProject__NetworkCatcher__) */
