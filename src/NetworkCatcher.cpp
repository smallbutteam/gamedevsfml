//
//  NetworkCatcher.cpp
//  GameProject
//
//  Created by Andrey Bondar on 10.11.14.
//  Copyright (c) 2014 Andrey Bondar. All rights reserved.
//

#include "builder.h"
#include "networkCatcher.h"


using namespace sf;

SBT::NetworkCatcher::NetworkCatcher(Network* pNetwork) {
    this->network = pNetwork;
}

bool SBT::NetworkCatcher::receiveSignal() {
    char command = this->network->reciveData();
    if( command == ' ' || command == 0 ) {
        return false;
    } else {
        lastData.push(command);
    }
    return true;
}

bool SBT::NetworkCatcher::moveLeft()
{
    if( !receiveSignal() ) {
        return false;
    }
    if( lastData.front() == 'L' ) {
        lastData.pop();
        return true;
    } else {
        return false;
    }
}

bool SBT::NetworkCatcher::moveRight()
{
    if( !receiveSignal() ) {
        return false;
    }
    if( lastData.front() == 'R' ) {
        lastData.pop();
        return true;
    } else {
        return false;
    }
}

bool SBT::NetworkCatcher::jump()
{
    if( !receiveSignal() ) {
        return false;
    }
    if( lastData.front() == 'J' ) {
        lastData.pop();
        return true;
    } else {
        return false;
    }
}

bool SBT::NetworkCatcher::showUp() {
    if( !receiveSignal() ) {
        return false;
    }
    if( lastData.front() == 'U' ) {
        lastData.pop();
        return true;
    } else {
        return false;
    }
}

bool SBT::NetworkCatcher::showLeft() {
    if( !receiveSignal() ) {
        return false;
    }
    if( lastData.front() == 'H' ) {
        lastData.pop();
        return true;
    } else {
        return false;
    }
}

bool SBT::NetworkCatcher::showRight() {
    if( !receiveSignal() ) {
        return false;
    }
    if( lastData.front() == 'K' ) {
        lastData.pop();
        return true;
    } else {
        return false;
    }
}

bool SBT::NetworkCatcher::showUpLeft() {
    if( !receiveSignal() ) {
        return false;
    }
    if( lastData.front() == 'Y' ) {
        lastData.pop();
        return true;
    } else {
        return false;
    }
}

bool SBT::NetworkCatcher::showUpRight() {
    if( !receiveSignal() ) {
        return false;
    }
    if( lastData.front() == 'I' ) {
        lastData.pop();
        return true;
    } else {
        return false;
    }
}