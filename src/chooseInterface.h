/* 
 * File:   chooseInterface.h
 * Author: rikimaru
 *
 * Created on 17 Декабрь 2014 г., 13:19
 */

#ifndef CHOOSEINTERFACE_H
#define	CHOOSEINTERFACE_H
#include "builder.h"

using namespace sf;

int chooseAction(RenderWindow& w);
std::string inputIp(RenderWindow& w);
bool validateIp(std::string& ip);
void waitForFriendWindow(RenderWindow& w);
#endif	/* CHOOSEINTERFACE_H */