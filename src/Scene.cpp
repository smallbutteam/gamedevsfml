//
//  Scene.cpp
//  GameProject
//
//  Created by Andrey Bondar on 09.11.14.
//  Copyright (c) 2014 Andrey Bondar. All rights reserved.
//

#include "scene.h"

SBT::Scene::Scene(int levelId) : CompanionLvl(0), PlayerLvl(0) {
    
    Connection = new Network();
    CompanionLvl = new Level(levelId, false, Connection);
    PlayerLvl = new Level(levelId, true, Connection);
    sync = new Sync(PlayerLvl, CompanionLvl, Connection);
}

void SBT::Scene::update(RenderWindow& window, Event& event)
{
	while( window.pollEvent(event) ) {
		if( event.type == Event::Closed ) {
			window.close();
		}
	}

    window.clear(Color::White);
    PlayerLvl->update(window, sync, true);
    CompanionLvl->update(window, sync, false);
    window.display();
   
}
