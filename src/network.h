//
//  Network.h
//  GameProject
//
//  Created by Andrey Bondar on 12.11.14.
//  Copyright (c) 2014 Andrey Bondar. All rights reserved.
//

#ifndef __GameProject__Network__
#define __GameProject__Network__

#include <SFML/Network.hpp>
#include "builder.h"

using namespace sf;

namespace SBT {
    class Network {
    public:
        Network();
        bool createHost();
        bool joinHost(std::string otherIP);
        void sendData(char Data);
        char reciveData();
        bool makeChoise();
    private:
        IpAddress myIP;
        TcpSocket Client;
        TcpListener listenHost;
        TcpSocket Host;        
    };
}






#endif /* defined(__GameProject__Network__) */
