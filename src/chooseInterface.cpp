#include "chooseInterface.h"

using namespace sf;

int chooseAction(RenderWindow& w) {
    
    Event event;
    Font f;
    
    if(!f.loadFromFile("../res/Arial Regular.Ttf"))
    {
        f.loadFromFile("res/Arial Regular.Ttf");
    }
    Text createText("Create", f);
    createText.setPosition(100, 20);
    createText.setColor(Color::Red);
    
    Text joinText("Join", f);
    joinText.setPosition(100, 200);
    joinText.setColor(Color::Black);
    
    int activeElem = 1;
    
    while (w.isOpen()){
        w.clear(Color::White);
        
        while( w.pollEvent(event) ) {
		if( event.type == Event::Closed ) {
                    w.close();
                    return 0;
		}
                if( Keyboard::isKeyPressed(Keyboard::Return)) {
                    return activeElem;
                }
                if( Keyboard::isKeyPressed(Keyboard::Down) ) {
                    activeElem = 2;
                    joinText.setColor(Color::Red);
                    createText.setColor(Color::Black);
                }
                if( Keyboard::isKeyPressed(Keyboard::Up)) {
                    activeElem = 1;
                    createText.setColor(Color::Red);
                    joinText.setColor(Color::Black);
                }
	}
        
    	w.draw(createText);
        w.draw(joinText);
        w.display();
    }
}


std::string inputIp(RenderWindow& w) {
    std::string ip;
    Font f;
    Event event;
    bool wrongAnswer = false;
    
    RectangleShape underline(Vector2f(240, 1));
    underline.setFillColor(Color::Black);
    underline.setPosition(100, 133);
    
    if(!f.loadFromFile("../res/Arial Regular.Ttf"))
    {
        f.loadFromFile("res/Arial Regular.Ttf");
    }
    
    Text inputIp("", f);
    Text hip("IP:", f);
    Text wrongIp("Ip are not correct!", f, 15);
    
    inputIp.setPosition(100, 100);
    hip.setPosition(55, 100);
    wrongIp.setPosition(10, 200);
    
    inputIp.setColor(Color::Black);
    hip.setColor(Color::Black);
    wrongIp.setColor(Color::Red);
    
    while( w.isOpen() ) {
        w.clear(Color::White);
        
        while( w.pollEvent(event) ) {
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Period) ) {
                ip += ".";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num0) ) {
                ip += "0";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num1) ) {
                ip += "1";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num2) ) {
                ip += "2";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num3) ) {
                ip += "3";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num4) ) {
                ip += "4";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num5) ) {
                ip += "5";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num6) ) {
                ip += "6";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num7) ) {
                ip += "7";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num8) ) {
                ip += "8";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num9) ) {
                ip += "9";
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::BackSpace) && ip.length() > 0 ) {
                ip.erase(ip.end()-1, ip.end());
            }
            if( (event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Return) ) {
                if( validateIp(ip) ) {
                    Text c("Connection...", f);
                    c.setColor(Color::Black);
                    c.setPosition(100, 20);
                    w.draw(c);
                    w.display();
                    return ip;
                } else {
                    ip.erase(ip.begin(), ip.end());
                    wrongAnswer = true;
                }
            }
            if( event.type == Event::Closed ) {
                w.close();
                return 0;
            }
	}
        
        if( wrongAnswer ) {
            w.draw(wrongIp);
        }
        inputIp.setString(ip);
        w.draw(hip);
    	w.draw(inputIp);
        w.draw(underline);
        w.display();
    }
}

bool validateIp(std::string& ip) {
    if( ip.length() == 0 ) {
        return false;
    }
    
    if( ip[0] == '.' ) {
        return false;
    }
    
    // Проверка на наличие 3 точек
    int cp = 0;
    for( int i = 0; i < ip.length(); i++ ) {
        if( ip[i] == '.' ) {
            cp++;
        }
    }
    if( cp != 3 ) {
        std::cout << "проверка на 3 точки" << std::endl;
        return false;
    }
    //=====================
    
    
    // Проверка на 2 точки подряд
    for( int i = 0; i < ip.length()-1; i++ ) {
        if( ip[i] == '.' && ip[i+1] == '.' ) {
            std::cout << "проверка на 2 точки подряд" << std::endl;
            return false;
        }
    }
    //===========================
    
    
    //Проверка на больше 3 цифр подряд
    int i = 0;
    int j = 0;
    for( i = 0; i < ip.length(); i++ ) {
        for( j = i; j < i+4 && j < ip.length(); j++ ) {
            if( j == i+3 && ip[j] != '.' ) {
                std::cout << "проверка на 4 цифры подряд" << std::endl;
                return false;
            }
            if( ip[j] == '.' ) {
                i = j;
                break;
            }
        }
    }
    
    return true;
    //============================
}

void waitForFriendWindow(RenderWindow& w) {
    std::string myIp = IpAddress::getLocalAddress().toString();
    
    Font f;
    if(!f.loadFromFile("../res/Arial Regular.Ttf"))
    {
        f.loadFromFile("res/Arial Regular.Ttf");
    }
    
    Text mp(myIp, f);
    mp.setColor(Color::Black);
    mp.setPosition(100, 70);

    Text wait("Wait for your friend...", f);
    wait.setColor(Color::Black);
    wait.setPosition(100, 20);

    w.draw(mp);
    w.draw(wait);
    w.display();
    return;
}