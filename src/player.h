/*
 * player.h
 *
 *  Created on: 08 нояб. 2014 г.
 *      Author: rscprof
 */

#ifndef PLAYER_H_
#define PLAYER_H_
#include "builder.h"
#include <SFML/Graphics.hpp>
#include "eventCatcher.h"


	namespace SBT {

		struct Coords{
			int x;
			int y;
			Coords(){
				x = 0; // АЙ-АЙ-АЙ, забываем инициализировать нулем)) меня выебали за это =)
				y = 0;
			}
		};

		enum Look{
			FRONT=1,
			RIGHT,
			LEFT
		};

		class Player {

		public:
			float dx,dy; // Ускорение по x и y
			float currentFrame; // кадр спрайта
			float* offsetX;
			float* offsetY;
            float ScreenStart;
            float cameraCalibration;
            
            
			bool onGround;
			bool buttonPressed;
			bool die;
            bool draw;
            
            int dying;

			sf::FloatRect rect;
			sf::Texture texture;
            //sf::Texture tiles;
			sf::Sprite sprite;
            //sf::Sprite showSp;
			std::string* TileMap;
			Look look;
			Coords respawn;

			Player(std::string* TileMap, float* offsetX, float* offsetY, bool isPlayer, b2World *World);

			void jump();
			void moveLeft();
			void moveRight();

			void update(float time, EventCatcher *catcher, bool flag, int& backgroundShift);
			void updateTexture(float time);

			void collision(char axis);

            b2Body *playerBody;
            b2World *world;
		};

	}



#endif /* PLAYER_H_ */
