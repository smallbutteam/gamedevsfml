/*
 * functions.h
 *
 *  Created on: 18.11.2014
 *      Author: rikimaru
 */

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include "builder.h"

struct binaryPare {
	float x;
	float y;
	binaryPare() {
		x = 0;
		y = 0;
	}
	binaryPare(float px, float py) : x(px), y(py) {};
};

struct fixtureParams {
	float density;
	float friction;
	float restriction;
	fixtureParams() : density(0), friction(0), restriction(0) {};
	fixtureParams(float d, float f, float r) {
		this->density = d;
		this->friction = f;
		this->restriction = r;
	}
};

struct b2BodyDefProperties{
	bool isFixedRotation;
	b2BodyType type;
	b2BodyDefProperties() : isFixedRotation(true), type(b2_staticBody) {};
	b2BodyDefProperties(bool isFixed, b2BodyType pType) {
		type = pType;
		isFixedRotation = isFixed;
	}
};

b2Body* createBox2dObject(b2World*, b2BodyDefProperties, binaryPare position, binaryPare shapeSize, fixtureParams);

namespace SBT {
    bool inArray(char* arr, char val, int size);
}

#endif /* FUNCTIONS_H_ */
