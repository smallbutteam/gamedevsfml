//
//  LocalCatcher.h
//  GameProject
//
//  Created by Andrey Bondar on 10.11.14.
//  Copyright (c) 2014 Andrey Bondar. All rights reserved.
//

#ifndef __GameProject__LocalCatcher__
#define __GameProject__LocalCatcher__

#include "eventCatcher.h"

namespace SBT {
    
    class LocalCatcher : public EventCatcher {
    public:
        LocalCatcher(Network* network);
        bool moveLeft();
        bool moveRight();
        bool jump();
        bool showUp();
        bool showLeft();
        bool showRight();
        bool showUpLeft();
        bool showUpRight();
    };
}

#endif /* defined(__GameProject__LocalCatcher__) */
