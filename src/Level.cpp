#ifndef level_cpp
#define level_cpp

#include "level.h"
#include "localCatcher.h"
#include "networkCatcher.h"

using namespace sf;

SBT::Level::Level(int levelId, bool isPlayer, Network* pConnection) : p(0) {

    this->world = new b2World(b2Vec2(0.0f, 10.0f));
    this->world->SetAllowSleeping(true);
    this->TileMap = TILE_MAP[levelId];
    this->connection = pConnection;
    this->offsetX = 0;
    this->offsetY = 0;
    this->backgroundShift = 0;
    this->backgroundY = 22;
    this->isLocal = isPlayer;
    
    if( !this->tileSet.loadFromFile("res/img/tiles01.bmp") ) {
        this->tileSet.loadFromFile("../res/img/tiles01.bmp");
    }
    if( !this->backgroundSet.loadFromFile("res/img/animate_background.png") ) {
        this->backgroundSet.loadFromFile("../res/img/animate_background.png");
    }
    this->background.setTexture(this->backgroundSet);
    
    this->s = new Signal();
    
    if( isPlayer ) {
       this->catcher = new LocalCatcher(pConnection);
       this->ScreenStart = HEIGHT_VISION;
       this->CameraCalibration = 0;
        this->s->signal.setPosition(50, 50);
    } else {
       this->catcher = new NetworkCatcher(pConnection);
       this->ScreenStart = 0;
        this->s->signal.setPosition(100, 50);
       this->CameraCalibration = 36;
    }
    background.setTextureRect(IntRect(10+this->backgroundShift,22 + this->ScreenStart,BACKGROUND_WIDTH-this->backgroundShift,32*12));
    for( int y = 0; y < H; y++ ) {
        for( int x = 0; x < W; x++ ) {
            if( this->TileMap[y][x] == 'B' ) {
                createBox2dObject(
                        this->world,
                        b2BodyDefProperties(true, b2_staticBody),
                        binaryPare(x*32 * METRE_TO_PIX, METRE_TO_PIX * (y*32 + this->ScreenStart)),
                        binaryPare(METRE_TO_PIX * 16.0f, METRE_TO_PIX * 16.0f),
                        fixtureParams(1.1f, 0.0f, 0.01f)
                );
            }
        }
    }

    for( int y = 0; y < H; y++ ) {
        for( int x = 0; x < W; x++ ) {
            if( this->TileMap[y][x] == '0' ) {
                createBox2dObject(
                  this->world,
                  b2BodyDefProperties(true, b2_staticBody),
                  binaryPare(x*32 * METRE_TO_PIX, METRE_TO_PIX * (y*32 + this->ScreenStart)),
                  binaryPare(METRE_TO_PIX * 16.0f, METRE_TO_PIX * 16.0f),
                  fixtureParams(1.2f, 0.0f, 0.01f)
                  );
            }
        }
    }
    
    for( int y = 0; y < H; y++ ) {
        for( int x = 0; x < W; x++ ) {
            if( this->TileMap[y][x] == 'S' ) {
                createBox2dObject(
                                  this->world,
                                  b2BodyDefProperties(true, b2_staticBody),
                                  binaryPare(x*32 * METRE_TO_PIX, METRE_TO_PIX * (y*32 + this->ScreenStart)),
                                  binaryPare(METRE_TO_PIX * 16.0f, METRE_TO_PIX * 16.0f),
                                  fixtureParams(1.3f, 0.0f, 0.01f)
                                  );
            }
        }
    }
    
    for( int y = 0; y < H; y++ ) {
        for( int x = 0; x < W; x++ ) {
            if( this->TileMap[y][x] == 'M' ) {
                createBox2dObject(
                                  this->world,
                                  b2BodyDefProperties(true, b2_staticBody),
                                  binaryPare(x*32 * METRE_TO_PIX, METRE_TO_PIX * (y*32 + this->ScreenStart)),
                                  binaryPare(METRE_TO_PIX * 16.0f, METRE_TO_PIX * 16.0f),
                                  fixtureParams(1.4f, 0.0f, 0.01f)
                                  );
            }
        }
    }
    
    
    for( int y = 0; y < H; y++ ) {
        for( int x = 0; x < W; x++ ) {
            if( this->TileMap[y][x] == 'D' ) {
                createBox2dObject(
                                  this->world,
                                  b2BodyDefProperties(false, b2_dynamicBody),
                                  binaryPare(x*32 * METRE_TO_PIX, METRE_TO_PIX * (y*32 + this->ScreenStart)),
                                  binaryPare(METRE_TO_PIX * 16.0f, METRE_TO_PIX * 16.0f),
                                  fixtureParams(0.01f, 0.00f, 0.01f)
                                  );
            }
        }
    }
    
    for( int y = 0; y < H; y++ ) {
        for( int x = 0; x < W; x++ ) {
            if( this->TileMap[y][x] == 'F' ) {
                createBox2dObject(
                                  this->world,
                                  b2BodyDefProperties(false, b2_dynamicBody),
                                  binaryPare(x*32 * METRE_TO_PIX, METRE_TO_PIX * (y*32 + this->ScreenStart)),
                                  binaryPare(METRE_TO_PIX * 16.0f, METRE_TO_PIX * 16.0f),
                                  fixtureParams(0.02f, 1.00f, 0.01f)
                                  );
            }
        }
    }
    
    for( int y = 0; y < H; y++ ) {
        for( int x = 0; x < W; x++ ) {
            if( this->TileMap[y][x] == 'H' ) {
                createBox2dObject(
                                  this->world,
                                  b2BodyDefProperties(true, b2_staticBody),
                                  binaryPare(x*32 * METRE_TO_PIX, METRE_TO_PIX * (y*32 + this->ScreenStart)),
                                  binaryPare(METRE_TO_PIX * 16.0f, METRE_TO_PIX * 16.0f),
                                  fixtureParams(1.1f, 0.01f, 0.01f)
                                  );
            }
        }
    }
    

    this->p = new Player(this->TileMap, &offsetX, &offsetY, isPlayer, this->world);
}

void SBT::Level::update(RenderWindow& window, Sync* sync,bool flag) {
        
	this->offsetX = 600;
	this->offsetY = 275 + this->ScreenStart + this->CameraCalibration;
	float time = this->clock.getElapsedTime().asMicroseconds();
	this->clock.restart();
	time = (time/700 > 20) ? 20 : time/700;
    float backgroundTime = this->backgroundClock.getElapsedTime().asSeconds();
    for (b2Body* dynIter = world->GetBodyList(); dynIter; dynIter = dynIter = dynIter->GetNext())
    {
        if (dynIter->GetType() == b2_dynamicBody)
        {
            b2Fixture* tempFix = dynIter->GetFixtureList();
            if (tempFix->GetDensity() == 0.02f)
            {
                float dy = dynIter->GetLinearVelocity().y;
                if (dy < 0.001f)
                {
                    dynIter->SetLinearVelocity(b2Vec2(0, 0));
                }
            }
        }
    }
    if (this->backgroundNumber > 4 || this->backgroundNumber < 0 ){
        this->backgroundNumber = 1;
    }
    if(backgroundTime > 1){
        this->backgroundNumber = this->backgroundNumber+1;
        this->backgroundY = (this->backgroundNumber == 2)?471:22;
        this->backgroundNumber = this->backgroundNumber % 3;
        this->backgroundClock.restart();
    }
    // Разрабы спрайта - мудаки. Ошиблись на 1 пиксель между третим и первым спрайтом
    background.setTextureRect(IntRect(((backgroundNumber==2)?11:10)+(((backgroundNumber==2)?0:backgroundNumber)*(BACKGROUND_WIDTH+7)),backgroundY,BACKGROUND_WIDTH,32*12));
    for (int i=-10; i<10 ; i++){ // Есть баг, упервшись в стену, можно прокрутить ленту полностью.
        // Исправляется проверкой изменении положения персонажа и в зависимости от этого менять backgroundShift
        // Или нет. Проверка так подозреваю где-то в box2D (Твой код не смотрел, решил время не терять,
        // возможно у нас она одинаковый код, при личной встрече - решим)
        background.setPosition((BACKGROUND_WIDTH-this->backgroundShift)+BACKGROUND_WIDTH*(i-1),this->ScreenStart-10);
        window.draw(background);
    }
	this->p->update(time, this->catcher, flag,this->backgroundShift);
        
        bool signalRecieved = false;
        
        if( this->catcher->showLeft() ) {
            this->s->signal.setTextureRect(IntRect(31, 0, ARROW_SIZE, ARROW_SIZE));
            signalRecieved = true;
        }
        if( this->catcher->showRight() ) {
            this->s->signal.setTextureRect(IntRect(0, 0, ARROW_SIZE, ARROW_SIZE));
            signalRecieved = true;
        }
        if( this->catcher->showUp() ) {
            this->s->signal.setTextureRect(IntRect(62, 0, ARROW_SIZE, ARROW_SIZE));
            signalRecieved = true;
        }
        if( this->catcher->showUpLeft() ) {
            this->s->signal.setTextureRect(IntRect(31, 31, ARROW_SIZE, ARROW_SIZE));
            signalRecieved = true;
        }
        if( this->catcher->showUpRight() ) {
            this->s->signal.setTextureRect(IntRect(0, 31, ARROW_SIZE, ARROW_SIZE));
            signalRecieved = true;
        }
        
        if( signalRecieved ) {
            this->s->timer.restart();
            this->s->showed = true;
        }
	this->redraw(window);
        
        if( this->s->isShowed() ) {
            window.draw(this->s->signal);
        }

}

void SBT::Level::redraw(RenderWindow& window) {
    for (b2Body* bodyIterator = this->world->GetBodyList(); bodyIterator != 0; bodyIterator = bodyIterator->GetNext()){
        if (bodyIterator->GetType() == b2_staticBody){
            b2Fixture* tempFix = bodyIterator->GetFixtureList();
            
            b2Vec2 vec = bodyIterator->GetPosition();
            Sprite sprite;
            sprite.setTexture(tileSet);
            if (tempFix->GetDensity() == 1.1f){
                
                if( tempFix->GetFriction()) {
                    if(!isLocal)
                    {
                        sprite.setTextureRect(IntRect(0,0,32,32));
                        sprite.setColor(Color::Cyan);
                    }
                    else {
                        sprite.setTextureRect(IntRect(0,0,0,0));
                    }
                }
                else {
                    sprite.setTextureRect(IntRect(0,0,32,32));
                }
            } else if (tempFix->GetDensity() == 1.2f) {
                sprite.setTextureRect(IntRect(32*0,7*32,32,32));
            } else if (tempFix->GetDensity() == 1.3f){
                sprite.setTextureRect(IntRect(32*8,3*32,32,32));
            } else if (tempFix->GetDensity() == 1.4f) {
                sprite.setTextureRect(IntRect(32*3, 32*3, 32, 32));
            } else {
                sprite.setTextureRect(IntRect(0,0,32,32));
            }
            b2Vec2 vec2;
            vec2.x = vec.x - this->p->playerBody->GetPosition().x + (this->offsetX - 16) * METRE_TO_PIX;
            vec2.y = vec.y - this->p->playerBody->GetPosition().y  + (this->offsetY - 16) * METRE_TO_PIX;
            sprite.setPosition(vec2.x * PIX_TO_METRE, vec2.y * PIX_TO_METRE);
            sprite.setRotation(bodyIterator->GetAngle() * 180/b2_pi);
            

//            if (vec2.x + 50 > 0 && vec2.y > this->ScreenStart - 36 && vec2.y < VIDEOMODE_Y/2 + this->ScreenStart - this->CameraCalibration )
//            {
            //if(isLocal || )
            window.draw(sprite);
//            }
        }
        else if (bodyIterator->GetType() == b2_dynamicBody)
        {
            b2Fixture* tempFix = bodyIterator->GetFixtureList();
            b2Vec2 vec = bodyIterator->GetPosition();
            Sprite sprite;
            //sprite.setTexture(tileSet);
            b2Vec2 vec2;
            if (tempFix->GetDensity() == 0.01f || tempFix->GetDensity() == 0.02f)
            {
                sprite.setTexture(tileSet);
                sprite.setTextureRect(IntRect(3*32, 10*32, 32, 32));
            }
            vec2.x = vec.x - this->p->playerBody->GetPosition().x + (this->offsetX) * METRE_TO_PIX;
            vec2.y = vec.y - this->p->playerBody->GetPosition().y  + (this->offsetY) * METRE_TO_PIX;
            sprite.setPosition(vec2.x * PIX_TO_METRE, vec2.y * PIX_TO_METRE);
            sprite.setRotation(bodyIterator->GetAngle() * 180/b2_pi);
            window.draw(sprite);
        }
    }

    Sprite sprite;
    sprite.setTexture(this->tileSet);
    for( int i = 0; i < W ; i++ ) {
        Sprite sprite;
        sprite.setTexture(this->tileSet);
        sprite.setPosition(i*32,HEIGHT_VISION-36);
        sprite.setTextureRect(IntRect(32*3, 32, 32, 32));
        window.draw(sprite);
    }
    Sprite skull;
    skull.setTexture(tileSet);
    skull.setPosition(0, HEIGHT_VISION-36);
    skull.setTextureRect(IntRect(32*9, 4*32, 32, 32));
    window.draw(skull);


    Sprite skull2;
    skull2.setTexture(tileSet);
    skull2.setPosition(VIDEOMODE_X - 32, HEIGHT_VISION-36);
    skull2.setTextureRect(IntRect(32*9, 4*32, 32, 32));
    window.draw(skull2);

    this->world->Step(1.0f/60.0f, 1, 1);

    window.draw(this->p->sprite);
    //window.draw(this->p->showSp);
    
}

SBT::Signal::Signal() {
    if( !this->signalsTileset.loadFromFile("res/img/arrows.png") ) {
        this->signalsTileset.loadFromFile("../res/img/arrows.png");
    }
    signal.setTexture(this->signalsTileset);
    showed = false;
}

bool SBT::Signal::isShowed() {
    if( this->showed && this->timer.getElapsedTime().asSeconds() > 3 ) {
        this->showed = false;
        this->timer.restart();
    }
    return showed;
}

#endif
