#ifndef builder_h
#define builder_h

#include <iostream>
#include <fstream>
#include <string>

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>

#include "network.h"
#include "settings.h"
#include "listOfLevels.h"
#include "functions.h"

#endif
