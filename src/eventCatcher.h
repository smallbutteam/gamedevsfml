//
//  EventCatcher.h
//  GameProject
//
//  Created by Andrey Bondar on 10.11.14.
//  Copyright (c) 2014 Andrey Bondar. All rights reserved.
//

#ifndef __GameProject__EventCatcher__
#define __GameProject__EventCatcher__

#include "builder.h"
#include "network.h"

namespace SBT {
    
    class EventCatcher {
    public:
        Network* network;
        virtual bool moveLeft() = 0;
        virtual bool moveRight() = 0;
        virtual bool jump() = 0;
        virtual bool showUp() = 0;
        virtual bool showLeft() = 0;
        virtual bool showRight() = 0;
        virtual bool showUpLeft() = 0;
        virtual bool showUpRight() = 0;
    };
    
}

#endif /* defined(__GameProject__EventCatcher__) */
