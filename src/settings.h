#ifndef settings_h
#define settings_h

	const int H = 14;
	const int W = 150;

	const float VIDEOMODE_X = 1200.0f;
	const float VIDEOMODE_Y = 800.0f;

	const float HEIGHT_VISION = VIDEOMODE_Y/2;

    const int BACKGROUND_WIDTH = 32*16-4;

	const float BLOCK_WIDTH = 32.0f;
	const float BLOCK_HEIGHT = 32.0f;

	const float PLAYER_WIDTH = 29.0f;
	const float PLAYER_HEIGHT = 49.0f;
    const float PIX_TO_METRE = 50.0f;
    const float METRE_TO_PIX = 0.02f;

    extern b2Vec2 gravity;
    extern b2World world;

#endif
